<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
<head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
</head>
<body>
<div id="base">
	<div id="header">
		<a href="<?php print check_url($base_path); ?>"><?php if ($logo) { print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" class="logo" />'; } ?></a>
		<h2><a href="<?php print check_url($base_path); ?>"><?php print check_plain($site_name); ?></a><br /><?php print check_plain($site_slogan); ?></h2>
	</div>
	<div id="secondaryMenu">
		<ul>
			<?php if (is_array($secondary_links)) : ?>
			<?php foreach($secondary_links AS $links){ echo '<li>'. l($links['title'], $links['href']). '</li>'; }
			endif; ?>
		</ul>
	</div>	
	<?php if (is_array($primary_links)) : ?>
		<div id="primaryMenu">
            <ul><?php foreach($primary_links AS $links){ echo '<li>'. l($links['title'], $links['href']). '</li>'; }; ?></ul>
			<?php print $search_box; ?>
		</div>
	<?php endif; ?>
	<div id="content">
		<div id="rightContent">
			<?php if ($sidebar_right): ?>
			<?php print $sidebar_right ?>
			<?php endif; ?>
			<div class="column"></div>
		</div>
		<div id="primaryContentContainer">
			<div id="primaryContent">
				<?php if ($mission != ""): ?>
				<div class="mission"><?php print $mission ?></div>
				<?php endif; ?>
				<?php if ($title != ""): ?>
				<h1><?php print $title ?></h1>
				<?php endif; ?>						
				<?php print $header; ?>					
				<?php if ($tabs != ""): ?>
				<?php print $tabs ?>
				<?php endif; ?>						
				<?php if ($help != ""): ?>
				<p id="help"><?php print $help ?></p>
				<?php endif; ?>
				<?php if ($messages != ""): ?>
				<div id="message"><?php print $messages ?></div>
				<?php endif; ?>
				<?php print($content) ?>
			</div>
		</div>
		<div id="leftContent">
			<?php if ($sidebar_left): ?>
			<?php print $sidebar_left ?>
			<?php endif; ?>
			<div class="column"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div id="footer">
		<?php print $footer_message ?>&nbsp;|&nbsp;<a href="http://www.dpol.ru/">relax</a>
	</div>
</div>
</body>
</html>
